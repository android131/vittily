package com.instagram.weightLog;

public class Exercise {

    private String name;
    private String id;

    public Exercise(String name) {
        this.name = name;
    }

    public Exercise(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
