package com.instagram.weightLog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.instagram.R;
import com.instagram.databinding.ActivityExerciseBinding;

import java.util.ArrayList;

public class ExerciseActivity extends AppCompatActivity implements ExerciseAdapter.OnItemClicked {

    ActivityExerciseBinding binding;
    private ExerciseAdapter adapter;
    private ArrayList<Exercise> exercises;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExerciseBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        exercises = new ArrayList<>();
        exercises.add(new Exercise("Bench Press"));
        exercises.add(new Exercise("Dumbbell  Press"));
        exercises.add(new Exercise("Dumbbell  fly"));
        exercises.add(new Exercise("Shoulder Press"));
        exercises.add(new Exercise("Lateral Raises"));
        exercises.add(new Exercise("Squats"));
        exercises.add(new Exercise("Leg Extension"));
        exercises.add(new Exercise("Pull Down"));
        exercises.add(new Exercise("Barbell Row"));
        exercises.add(new Exercise("Machine Row"));
        exercises.add(new Exercise("Pull Ups"));
        exercises.add(new Exercise("Barbell Curl"));
        exercises.add(new Exercise("Dumbbell Curl"));
        exercises.add(new Exercise("Rope Pushdown"));
        exercises.add(new Exercise("Dumbbell Extension"));

        adapter = new ExerciseAdapter(exercises);
        binding.recyclerView.setAdapter(adapter);
        adapter.setOnClick(ExerciseActivity.this);

    }

    @Override
    public void onItemClick(int position) {
        Log.i("test", "You clicked " + exercises.get(position).getName());
        String exerciseName = exercises.get(position).getName();
        Intent intent = new Intent(ExerciseActivity.this, ExerciseInfo.class);
        intent.putExtra("exerciseName", exerciseName);
        startActivity(intent);
    }
}
