package com.instagram.weightLog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.instagram.R;
import com.instagram.databinding.ActivityExerciseInfoBinding;

import java.util.Objects;

public class ExerciseInfo extends AppCompatActivity {

    ActivityExerciseInfoBinding binding;
    SharedPreferences sharedPreferences;
    String savedSets, savedReps, savedWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExerciseInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();

        if (bundle != null) {
            String name = bundle.getString("exerciseName");
            binding.tvExerciseName.setText(name);
        }

        sharedPreferences = this.getSharedPreferences(getPackageName(), MODE_PRIVATE);

        savedSets = sharedPreferences.getString("sets", null);
        savedReps = sharedPreferences.getString("reps", null);
        savedWeight = sharedPreferences.getString("weight", null);

        if (!Objects.requireNonNull(sharedPreferences.getString("sets", "sets")).isEmpty()) {
            binding.setsInput.setText(savedSets);
        }
        if (!Objects.requireNonNull(sharedPreferences.getString("reps", "reps")).isEmpty()) {
            binding.repsInput.setText(savedReps);
        }
        if (!Objects.requireNonNull(sharedPreferences.getString("weight", "weight")).isEmpty()) {
            binding.weightInput.setText(savedWeight);
        }

        binding.btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sets = getSetsInput();
                String reps = getRepsInput();
                String weight = getWeightInput();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("sets", sets);
                editor.putString("reps", reps);
                editor.putString("weight", weight);
                editor.apply();

                Toast toast = Toast.makeText(ExerciseInfo.this, "Input Saved", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM, 0, 350);
                toast.show();
            }
        });
    }

    private String getSetsInput() {
        if (binding.setsInput.getText().toString().isEmpty()) {
            return "0";
        } else {
            return binding.setsInput.getText().toString().trim();
        }
    }

    private String getRepsInput() {
        if (binding.repsInput.getText().toString().isEmpty()) {
            return "0";
        } else {
            return binding.repsInput.getText().toString().trim();
        }
    }

    private String getWeightInput() {
        if (binding.weightInput.getText().toString().isEmpty()) {
            return "0";
        } else {
            return binding.weightInput.getText().toString().trim();
        }
    }
}
