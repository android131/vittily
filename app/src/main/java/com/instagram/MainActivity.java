package com.instagram;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.instagram.databinding.ActivityMainBinding;
import com.instagram.stopwatch.StopWatchActivity;
import com.instagram.timer.TimerActivity;
import com.instagram.weightLog.ExerciseActivity;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    Button btSplash, btTimer, btTracker;
    Animation atg, btgOne, btgtwo;
    ImageView ivSplash;
    TextView tvSplash, tvSubSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        btSplash = binding.btSplash;
        btTimer = binding.btTimer;
        btTracker = binding.btTracker;
        ivSplash = binding.ivSplash;
        tvSplash = binding.tvSplash;
        tvSubSplash = binding.tvSubSplash;

        atg = AnimationUtils.loadAnimation(this, R.anim.atg);
        btgOne = AnimationUtils.loadAnimation(this, R.anim.btgone);
        btgtwo = AnimationUtils.loadAnimation(this, R.anim.btgtwo);

        ivSplash.startAnimation(atg);
        tvSplash.startAnimation(btgOne);
        tvSubSplash.startAnimation(btgOne);
        btSplash.startAnimation(btgtwo);
        btTimer.startAnimation(btgtwo);
        btTracker.startAnimation(btgtwo);

        btSplash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, StopWatchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        btTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, TimerActivity.class);
                startActivity(intent);
            }
        });

        btTracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ExerciseActivity.class);
                startActivity(intent);
            }
        });
    }
}


