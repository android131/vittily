package com.instagram.timer;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instagram.R;
import com.instagram.databinding.ActivityTimerBinding;

public class TimerActivity extends AppCompatActivity {

    private ActivityTimerBinding binding;
    private EditText etMinutes, etSeconds;
    private ImageButton btStart, btStop, btPause;
    private TextView tvTimer;
    private CountDownTimer timer;
    private LinearLayout layout, layoutButtons;
    private Animation btgTwo;
    private long timeRemaining = 0;
    private boolean isPaused;
    private MediaPlayer mp;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTimerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        etMinutes = binding.etMinutes;
        etSeconds = binding.etSeconds;
        btStart = binding.btStart;
        btStop = binding.btStop;
        btPause = binding.btPause;
        tvTimer = binding.tvTimer;
        layoutButtons = binding.layoutButtons;

        btgTwo = AnimationUtils.loadAnimation(this, R.anim.btgtwo_timer_animation);
        layoutButtons.setAnimation(btgTwo);

        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String minutes, seconds;

                if (TextUtils.isEmpty(etMinutes.getText().toString())) {
                    etMinutes.setText("0");
                    minutes = etMinutes.getText().toString();
                } else {
                    minutes = etMinutes.getText().toString();
                }

                if (TextUtils.isEmpty(etSeconds.getText().toString())) {
                    etSeconds.setText("0");
                    seconds = etSeconds.getText().toString();
                } else {
                    seconds = etSeconds.getText().toString();
                }

                long time = getTimeFromInput(minutes, seconds);

                tvTimer.setText(minutes + ":" + seconds);
                showTimer();

                if (isPaused) {
                    startTimer(timeRemaining);
                    isPaused = false;
                } else {
                    startTimer(time + 1000);
                }
            }
        });

        btPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                btStart.setEnabled(true);
                isPaused = true;
            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.etSeconds.setText("");
                binding.etMinutes.setText("");
                showInput();
                timer.cancel();
                btStart.setEnabled(true);
                isPaused = false;
                mp.stop();

            }
        });
    }

    private void startTimer(final long time) {
        timer = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                tvTimer.setText(getString(R.string.timer_countdown) + millisUntilFinished / 1000);

                timeRemaining = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                tvTimer.setText(R.string.timer_finished);
                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                mp = MediaPlayer.create(getApplicationContext(), uri);
                mp.start();
            }
        }.start();

        btStart.setEnabled(false);
    }

    private void showInput() {
        binding.inputLayout.setVisibility(View.VISIBLE);
        binding.tvTimer.setVisibility(View.INVISIBLE);
    }

    private void showTimer() {
        binding.inputLayout.setVisibility(View.INVISIBLE);
        binding.tvTimer.setVisibility(View.VISIBLE);
    }

    private long getTimeFromInput(String minutes, String seconds) {

        try {
            long min = Integer.parseInt(minutes) * 60_000L;
            long sec = Integer.parseInt(seconds) * 1000L;
            return min + sec;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

}
