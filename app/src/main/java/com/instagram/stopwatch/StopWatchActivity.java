package com.instagram.stopwatch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;

import com.instagram.R;
import com.instagram.databinding.ActivityStopWatchBinding;

public class StopWatchActivity extends AppCompatActivity {

    ActivityStopWatchBinding binding;

    Button btStart, btStop;
    ImageView ivCircle, ivAnchor;
    Animation roundingAnchor;
    Chronometer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityStopWatchBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        btStart = binding.btStart;
        btStop = binding.btStop;
        ivCircle = binding.ivCircle;
        ivAnchor = binding.ivAnchor;
        timer = binding.timer;

        roundingAnchor = AnimationUtils.loadAnimation(this, R.anim.rounding_anchor);

        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivAnchor.startAnimation(roundingAnchor);
                btStop.setVisibility(View.VISIBLE);
                btStart.setVisibility(View.INVISIBLE);
                timer.setBase(SystemClock.elapsedRealtime());
                timer.start();
            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivAnchor.clearAnimation();
                btStop.setVisibility(View.INVISIBLE);
                btStart.setVisibility(View.VISIBLE);
                timer.stop();
            }
        });
    }
}
